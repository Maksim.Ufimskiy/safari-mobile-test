import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { StudentListComponent } from '@sm-components/student-list/student-list.component';
import { StudentsResoverService } from '@sm-services/students-resolver.service';
import { GradeAggregationResoverService } from '@sm-services/grade-aggregation-resolver.service';
import { GradeAggregationComponent } from '@sm-components/grade-average/grade-average.component';

export const appRoutes: Routes = [
    {
        path: 'top-students',
        component: StudentListComponent,
        data: {
            title: 'Top Students(Only Grade \'A\')',
            state: 'topStudents',
            refreshOnEdit: true
        },
        resolve: {
            students: StudentsResoverService
        }
    },
    {
        path: 'students',
        component: StudentListComponent,
        data: {
            title: 'All Students'
        },
        resolve: {
            students: StudentsResoverService
        }
    },
    {
        path: 'average',
        component: GradeAggregationComponent,
        data: {
            title: 'Grade Aggregation'
        },
        resolve: {
            gradeAggregation: GradeAggregationResoverService
        }
    },
    {
        path: '**',
        redirectTo: '/top-students',
        pathMatch: 'full'
    }
];

@NgModule({
    imports: [
        RouterModule.forRoot(
            appRoutes
        )
    ],
    exports: [
        RouterModule
    ]
})
export class AppRoutingModule { }