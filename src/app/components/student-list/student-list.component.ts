import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { IStudent, StudentsService } from '@sm-services/students.service';

@Component({
    selector: 'sm-student-list',
    templateUrl: './student-list.component.html',
    styleUrls: ['./student-list.component.css']
})
export class StudentListComponent {
    studentList: IStudent[];
    title: string;
    displayedColumns: string[] = ['id', 'firstname', 'lastname', 'grade'];
    selectedStudent: IStudent;
    refreshOnEdit: boolean;

    constructor(private route: ActivatedRoute, private studentsService: StudentsService, private router: Router) { }

    ngOnInit() {
        this.route.data
            .subscribe((data: { students: IStudent[], title: string, refreshOnEdit: boolean }) => {
                this.studentList = data.students;
                this.title = data.title;
                this.refreshOnEdit = data.refreshOnEdit;
            });
    }

    selectStudent(student: IStudent) {
        this.selectedStudent = (!this.selectedStudent || student.id !== this.selectedStudent.id) ? student : null;
    }

    changeGrade(grade: string | null): void {
        if (grade) {
            this.selectedStudent.grade = grade;
        }

        this.selectedStudent = null;

        if (this.refreshOnEdit) {
            this.studentsService.getTopStudents().subscribe((students: IStudent[]) => {
                this.studentList = students;
            });
        }
    }
}
