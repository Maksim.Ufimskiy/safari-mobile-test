import { Component, Input, Output, EventEmitter } from '@angular/core';
import { IStudent } from '@sm-services/students.service';

@Component({
    selector: 'sm-student-card',
    templateUrl: './student-card.component.html',
    styleUrls: ['./student-card.component.css']
})
export class StudentCardComponent {

    @Input() student: IStudent;
    @Output() gradeChange = new EventEmitter<string>();

    grades: string[] = ['A', 'B', 'C', 'D', 'E', 'F'];
    studentGrade: string;

    constructor() { }

    ngOnInit() {
        this.studentGrade = this.student.grade;
    }

    save(): void {
        this.gradeChange.emit(this.studentGrade);
    }

    cancel(): void {
        this.gradeChange.emit(null);
    }
}
