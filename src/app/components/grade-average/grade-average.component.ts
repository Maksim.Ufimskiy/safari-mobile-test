import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';


@Component({
    selector: 'sm-grade-average',
    templateUrl: './grade-average.component.html',
    styleUrls: ['./grade-average.component.css']
})
export class GradeAggregationComponent {

    gradeAggregation: any;
    title: string;
    displayedColumns: string[] = ['grade', 'numOfStudents'];

    constructor(private route: ActivatedRoute) { }

    ngOnInit() {
        this.route.data
            .subscribe((data: { gradeAggregation: any, title: string }) => {
                const averageGradeTableData = [];
                this.title = data.title;

                for (let key in data.gradeAggregation) {
                    let value = data.gradeAggregation[key];
                    averageGradeTableData.push({
                        grade: key,
                        value
                    });
                }

                this.gradeAggregation = averageGradeTableData.sort((a, b) => b.value - a.value);
            });
    }
}
