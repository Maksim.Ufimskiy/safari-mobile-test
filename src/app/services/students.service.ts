import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';
@Injectable({
    providedIn: 'root'
})
export class StudentsService {
    private students$: BehaviorSubject<IStudent[]> = new BehaviorSubject<IStudent[]>(students);

    public getStudents(): Observable<IStudent[]> {
        return this.students$;
    }

    public getTopStudents(): Observable<IStudent[]> {
        return this.getStudents().pipe(
            map(students => students.filter(student => student.grade === 'A'))
        );
    }

    public getGradesAggregation(): Observable<any> {
        return this.getStudents().pipe(
            map(students => students.reduce((aggregation: any, student: IStudent) => {
                aggregation[student.grade] = !aggregation[student.grade] ? 1 : aggregation[student.grade] + 1;
                return aggregation;
            }, {}))
        );
    }
}

export interface IStudent {
    id: number;
    firstname: string;
    lastname: string;
    grade: string;
}

const students: IStudent[] = [
    {
        id: 1,
        firstname: 'Daniel',
        lastname: 'Howcroft',
        grade: 'A',
    },
    {
        id: 2,
        firstname: 'Robert',
        lastname: 'Taylor',
        grade: 'B',
    },
    {
        id: 3,
        firstname: 'Christian',
        lastname: 'Brown',
        grade: 'F',
    },
    {
        id: 4,
        firstname: 'Mathew',
        lastname: 'Smith',
        grade: 'C',
    },
    {
        id: 5,
        firstname: 'Thomas',
        lastname: 'Marsden',
        grade: 'A',
    },
    {
        id: 6,
        firstname: 'John',
        lastname: 'Mcafee',
        grade: 'C',
    },
    {
        id: 7,
        firstname: 'Rihanna',
        lastname: 'Fenty',
        grade: 'A',
    },
    {
        id: 8,
        firstname: 'Madonna',
        lastname: 'Ciccone',
        grade: 'B',
    },
    {
        id: 9,
        firstname: 'Eric',
        lastname: 'Filton',
        grade: 'C',
    },
    {
        id: 10,
        firstname: 'Greg',
        lastname: 'Wallace',
        grade: 'D',
    }
];
