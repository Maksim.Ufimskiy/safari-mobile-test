import { Injectable } from '@angular/core';
import { Resolve, Router } from '@angular/router';
import { Observable, EMPTY, of } from 'rxjs';
import { take, mergeMap } from 'rxjs/operators';
import { StudentsService } from '@sm-services/students.service';

@Injectable({
    providedIn: 'root'
})
export class GradeAggregationResoverService implements Resolve<any> {

    constructor(private studentsService: StudentsService, private router: Router) { }

    resolve(): Observable<any> | Observable<never> {
        return this.studentsService.getGradesAggregation().pipe(
            take(1),
            mergeMap(aggregation => {
                if (aggregation) {
                    return of(aggregation);
                } else {
                    this.router.navigate(['/top-students']);
                    return EMPTY;
                }
            })
        );
    }
}