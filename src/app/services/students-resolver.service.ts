import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { take, mergeMap } from 'rxjs/operators';
import { StudentsService, IStudent } from '@sm-services/students.service';

@Injectable({
    providedIn: 'root'
})
export class StudentsResoverService implements Resolve<IStudent[]> {

    constructor(private studentsService: StudentsService) { }

    resolve(route: ActivatedRouteSnapshot): Observable<IStudent[]> | Observable<never> {
        const studentsSource = route.data.state === 'topStudents' ? this.studentsService.getTopStudents() : this.studentsService.getStudents();
        return studentsSource.pipe(
            take(1),
            mergeMap(students => {
                if (students) {
                    return of(students);
                } else {
                    return EMPTY;
                }
            })
        );
    }
}