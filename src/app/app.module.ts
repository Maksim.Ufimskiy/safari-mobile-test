import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatButtonModule } from '@angular/material/button';
import { MatRippleModule } from '@angular/material/core';
import { MatDividerModule } from '@angular/material/divider';
import { MatTableModule } from '@angular/material/table';
import { MatCardModule } from '@angular/material/card';
import { MatSelectModule } from '@angular/material/select';

import { AppComponent } from './app.component';
import { StudentsService } from '@sm-services/students.service';
import { StudentListComponent } from '@sm-components/student-list/student-list.component';
import { StudentCardComponent } from '@sm-components/student-card/student-card.component';
import { AppRoutingModule } from './app-routing.module';
import { GradeAggregationComponent } from '@sm-components/grade-average/grade-average.component';

@NgModule({
    declarations: [
        AppComponent,
        StudentListComponent,
        StudentCardComponent,
        GradeAggregationComponent
    ],
    imports: [
        AppRoutingModule,
        BrowserModule,
        BrowserAnimationsModule,
        MatToolbarModule,
        MatSidenavModule,
        MatButtonModule,
        MatRippleModule,
        MatDividerModule,
        MatTableModule,
        MatCardModule,
        MatSelectModule
    ],
    providers: [
        StudentsService
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
