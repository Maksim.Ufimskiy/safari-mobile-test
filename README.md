# SafariMobileTest

## Assumptions
1. List of students is stored in memory which means that after app restart, list of students is reset to initial value.
2. There is no clear criteria in requirements that telling how to select 3 top students in situation when there are more than 3 students with 'A' Grade. So I decided to display all students with 'A' Grade on `/top-student` page instead of picking 3 of them randomly.

## Prepairing the project
Run `npm install` in the root folder to install dependencies.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.


If you experience any issues with `ng` command, try to install angular-cli globally by executing `npm install -g @angular/cli`